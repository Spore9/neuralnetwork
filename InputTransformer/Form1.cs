﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace InputTrasformer
{
    public partial class Form1 : Form
    {
        bool regression = true;
        int inputs = 12;
        int sum = 0;
        List<DateTime> dates = new List<DateTime>();
        Dictionary<string, int> namePopularity = new Dictionary<string, int>();
        List<float> maxInput = new List<float>();
        Dictionary<int, int> namesEntry = new Dictionary<int, int>();
        List<List<float>> X = new List<List<float>>();
        List<float> boxes = new List<float>();
        int minimumYear = 1915;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.ShowDialog();
        }

        private void improveValues(ref List<List<float>> X, int m, List<string[]> names)
        {
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < inputs; j++)
                {
                    switch (j)
                    {
                        case 0:
                            break;
                        case 1: X[i][j] = nameConvertion(names[namesEntry[(int)X[i][0]]]);
                            break;
                        case 4: X[i][j] = dateConversion(i);
                            break;
                        default: X[i][j] /= maxInput[j];
                            break;
                    }
                }
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
            {
                int m = 0;
                List<string[]> movieNames = new List<string[]>();
                while (!sr.EndOfStream)
                {
                    if (regression)
                    {
                        if (m != 0)
                        {
                            for (int i = 0; i < inputs; i++)
                            {
                                if (X[m - 1][i] > maxInput[i])
                                    maxInput[i] = X[m - 1][i];
                            }
                        }
                        else
                        {
                            for (int i = 0; i < inputs; i++)
                            {
                                maxInput.Add(0);
                            }
                        }
                    }
                    X.Add(new List<float>());
                    string[] str = sr.ReadLine().Split('\t');
                    for (int j = 0; j < inputs; j++)
                    {
                        switch (j)
                        {
                            case 1:
                                X[m].Add(0);
                                bool allow = true;
                                if (m!=0)
                                {
                                    if (X[m][0] == X[m - 1][0])
                                    {
                                        allow = false;
                                    }
                                }   
                                if (allow)
                                {
                                    string[] names = str[j].Split(' ');
                                    addNamesEntry((int)X[m][0], movieNames);
                                    movieNames.Add(names);
                                    for (int k = 0; k < names.Count(); k++)
                                    {
                                        countNames(names[k]);
                                    }
                                }         
                                break;
                            case 3: boxes.Add(float.Parse(str[j]));
                                X[m].Add(0);
                                break;
                            case 4:
                                X[m].Add(0);
                                getDate(str[j]);
                                break;
                            case 6: X[m].Add(float.Parse(str[j])*boxes[m]);
                                break;
                            case 7: X[m].Add(getGenreNumber(str[j]));
                                break;
                            case 8:
                                X[m].Add(getRatingNumber(str[j]));
                                break;
                            case 9:
                                X[m].Add(getOriginNumber(str[j]));
                                break;
                            case 10:
                                X[m].Add(getCreativeTypeNumber(str[j]));
                                break;
                            case 11:
                                X[m].Add(int.Parse(str[j]));
                                break;
                            default: X[m].Add(int.Parse(str[j]));
                                break;
                        }
                    }
                    m++;
                }
                if (regression)
                    improveValues(ref X, m, movieNames);
                MessageBox.Show("Успешно преобразовано");
                saveFileDialog1.ShowDialog();
            }
        }
        private int getRatingNumber(string rating)
        {
            if (rating == "R")
                return 1;
            if (rating == "PG-13")
                return 2;
            if (rating == "PG")
                return 3;
            return 0;
        }
        private int getOriginNumber(string origin)
        {
            if (origin == "Original Screenplay")
                return 1;
            if (origin == "Based on Factual Book/Article")
                return 2;
            if (origin == "Based on Comic/Graphic Novel")
                return 3;
            if (origin == "Based on Musical or Opera")
                return 4;
            if (origin == "Remake")
                return 5;
            if (origin == "Based on Fiction Book/Short Story")
                return 6;
            if (origin == "Based on Theme Park Ride")
                return 7;
            if (origin == "Based on Real Life Events")
                return 8;
            return 0;
        }
        private int getGenreNumber(string genre)
        {
            if (genre == "Drama")
                return 1;
            if (genre == "Comedy")
                return 2;
            if (genre == "Documentary")
                return 3;
            if (genre == "Adventure")
                return 4;
            if (genre == "Action")
                return 5;
            if (genre == "Musical")
                return 6;
            if (genre == "Thriller")
                return 7;
            return 0;
        }
        private int getCreativeTypeNumber(string type)
        {
            if (type == "Dramatization")
                return 1;
            if (type == "Contemporary Fiction")
                return 2;
            if (type == "Super Hero")
                return 3;
            if (type == "Historical Fiction")
                return 4;
            if (type == "Fantasy")
                return 5;
            if (type == "Science Fiction")
                return 6;
            if (type == "Kids Fiction")
                return 7;
            if (type == "Factual")
                return 8;
            return 0;
        }
        private void addNamesEntry(int number, List<string[]> names)
        {
            if (!namesEntry.ContainsKey(number))
            {
                namesEntry.Add(number, names.Count);
            }
        }
        private float nameConvertion(string[] names)
        {
            float popularity = 0;
            for (int i=0;i<names.Count();i++)
            {
                string tmpName = transfomString(names[i]);
                if (isSuitName(tmpName) && namePopularity.ContainsKey(tmpName))
                {
                    popularity += (float)namePopularity[tmpName] / sum;
                }
            }
            if (popularity>1)
                return 1;
            return popularity;
        }
        private bool isSuitName(string name)
        {
            if (name != "the" && name != "and" && name != "a" && name != "with" && name != "in" && name != "or" && name != "at" && name != "of")
                return true;
            else
                return false;
        }
        private string transfomString(string inputString)
        {
            string outputString = inputString;
            outputString = outputString.ToLower();
            outputString = outputString.Replace(",", "");
            outputString = outputString.Replace("/", "");
            outputString = outputString.Replace("\"", "");
            outputString = outputString.Replace(":", "");
            return outputString;
        }
        private void countNames(string name)
        {
            string tmpName = transfomString(name);
            if (namePopularity.ContainsKey(tmpName))
            {
                namePopularity[tmpName]++;
                sum++;
                
            }
            else if (isSuitName(tmpName))
            {
                namePopularity.Add(tmpName, 1);
                sum++;
            }
        }
        private void getDate(string date)
        {
            DateTime tmp = new DateTime();
            string[] formats = { "MM/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM.dd.yyyy", "M.d.yyyy", "MM.d.yyyy", "M.dd.yyyy", "dd.MM.yyyy", };
            if (!DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out tmp))
            {
                MessageBox.Show("Не удалось "+date);
                return;
            }
            dates.Add(tmp);
        }
        private float dateConversion(int i)
        {
            DateTime tmp = dates[i];
            return (float) 1/((int)(tmp.DayOfWeek)+1);
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
            {
                for (int m= 0; m < X.Count; m++)
                {
                    for (int i = 1; i < inputs; i++)
                    {
                        switch (i)
                        {
                            case 1: sw.Write(X[m][i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                break;
                            case 3:
                                break;
                            case 4: DateTime tmp = dates[m];
                                float month = (float)1 / (tmp.Month+1);
                                float year = (float)1 / (tmp.Year-minimumYear + 1);
                                sw.Write('\t' + X[m][i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + month.ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + year.ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + X[m][i + 3].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + X[m][i + 4].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + X[m][i + 5].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + X[m][i + 6].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                sw.Write('\t' + X[m][i + 7].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                break;
                            case 6:
                                if (m != 0)
                                {
                                    if (X[m][0] != X[m - 1][0])
                                    {
                                        sw.Write('\t' + "0");
                                    }
                                    else
                                        sw.Write('\t' + X[m - 1][i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                }
                                else
                                    sw.Write('\t' + "0");
                                sw.Write('\t' + X[m][i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                break;
                            default:    sw.Write('\t' + X[m][i].ToString(System.Globalization.CultureInfo.InvariantCulture));
                                break;
                        }
                    }
                    sw.Write(Environment.NewLine);
                }
            }
        }
    }
}
