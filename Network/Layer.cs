﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANN
{
    [Serializable]
    class Layer
    {
        public List<List<float>> Weights;
        public List<float> Activations;
        Random rnd = new Random();
        float epsilon = 0.1f;
        public Layer(int countInputs, int countOutput)
        {
            Random rnd = new Random();
            Weights = new List<List<float>>();
            Activations = new List<float>();
            Activations.Add(1); //Bias
            for (int i = 0; i < countOutput; i++)
            {
                Weights.Add(new List<float>());
                Activations.Add(0);
                for (int j = 0; j < countInputs + 1; j++)
                {
                    //Weights[i].Add((float)(rnd.NextDouble() * (2 * epsilon) - epsilon));
                    Weights[i].Add((float)((rnd.NextDouble()*2-1)*(Math.Sqrt(6d) / Math.Sqrt(countInputs + countOutput))));
                }
            }
        }
    }
}
