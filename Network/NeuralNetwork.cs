﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANN
{
    [Serializable]
    class NeuralNetwork
    {
        float lambda = 1; //коэффициент регуляризации
        int m = 0; //количество обучающих примеров
        float a = 0.0001f; //скорость обучения
        private List<Layer> _layers; //слои
        public List<float> Inputs; //входные значения
        private bool _stop;
        public void ForwardPropagation()
        {
            for (int i = 0; i < _layers.Count; i++)
            {
                if (i == 0)
                {
                    if (Inputs.Count != _layers[i].Weights[i].Count)
                        Inputs.Insert(i, 1);
                    _layers[i].Activations = matrixVectorMultiplay(Inputs, _layers[i].Weights);
                }
                else
                {
                    if (_layers[i - 1].Activations.Count != _layers[i].Weights[0].Count)
                        _layers[i - 1].Activations.Insert(0, 1);
                    else
                        _layers[i - 1].Activations[0] = 1;
                    _layers[i].Activations = matrixVectorMultiplay(_layers[i - 1].Activations, _layers[i].Weights);
                }
                sigmoid(ref _layers[i].Activations);
            }
        } //Прохождение вперёд
        public int LearnNetwork(List<List<float>> X, List<List<float>> Y, int iter, float alpha,float lambd)
        {
            m = X.Count;
            a = alpha;
            lambda = lambd;
            float PreviousJ = 0;
            float J = 0;
            for (int i = 0; i < iter; i++)
            {
                List<List<float>> h = new List<List<float>>();
                List<List<List<float>>> activations = new List<List<List<float>>>();
                OnNewIteration(i, J);
                if (_stop)
                {
                    _stop = false;
                    return 2;
                }
                for (int j = 0; j < m; j++) //рассчитываем гипотезы
                {
                    Inputs = X[j];
                    ForwardPropagation();
                    h.Add(_layers.Last().Activations);
                    activations.Add(new List<List<float>>());
                    for (int k=0;k<_layers.Count;k++)
                    {
                        activations[j].Add(_layers[k].Activations);
                    }
                }
                PreviousJ = J;
                J = getCostFunction(h, Y);
                if (i != 0)
                    if (J > PreviousJ)
                    {
                        return 1;
                    }
                backPropagation(Y, X,activations);
            }
            return 0;
        } //обучение
        public NeuralNetwork(int numberOfInputs, int numberOfLayers, int numberOfUnits, int numberOfOutputs)
        {
            _layers = new List<Layer>();
            Inputs = new List<float>();
            _stop = false;
            for (int i = 0; i < numberOfInputs + 1; i++)
                Inputs.Add(0);
            Inputs[0] = 1;
            for (int i = 0; i < numberOfLayers; i++)
            {
                if (i == 0)
                {
                    if (numberOfLayers == 1)
                        _layers.Add(new Layer(numberOfInputs, numberOfOutputs));
                    else
                        _layers.Add(new Layer(numberOfInputs, numberOfUnits));
                }
                else
                {
                    if (i == numberOfLayers - 1)
                    {
                        _layers.Add(new Layer(numberOfUnits, numberOfOutputs));
                    }
                    else
                        _layers.Add(new Layer(numberOfUnits, numberOfUnits));
                }
            }
        } //конструктор
        public List<float> GetAswer()
        {
            return _layers.Last().Activations;
        } //Получить результат
        public int GetOutputs()
        {
            return _layers.Last().Weights.Count;
        } //Получить число выходов
        public int GetInputs()
        {
            return _layers[0].Weights[0].Count;
        } //Получить число входов
        public int GetLayersCount()
        {
            return _layers.Count;
        }
        public int GetNeuronsCount()
        {
                return _layers[0].Weights[0].Count;
        }
        public List<List<float>> GetWeight(int layer)
        {
            return _layers[layer].Weights;
        }
        private void backPropagation(List<List<float>> Y, List<List<float>> X, List<List<List<float>>> activations)
        {
            List<List<List<float>>> derivatives = getDerivatives(Y, X,activations);
            gradientDescent(derivatives);
        } //Обратное распространение ошибки
        private float getRegularisation()
        {
            float regularization = (float)(lambda / (2 * m));
            if (regularization != 0)
            {
                for (int L = 0; L < _layers.Count; L++)
                {
                    for (int i = 0; i < _layers[L].Weights.Count; i++)
                    {
                        for (int j = 0; j < _layers[L].Weights[i].Count; j++)
                        {
                            if (j != 0)
                                regularization += (float)Math.Pow(_layers[L].Weights[i][j], 2);
                        }
                    }

                }
            }
            return regularization;
        } //Получение коэф регуляризации для J
        private float getCostFunction(List<List<float>> h, List<List<float>> Y)
        {
            float J = 1f / Y.Count;
            float regularization = getRegularisation();
            float sum = 0;
            for (int i = 0; i < Y.Count; i++)
                for (int k = 0; k < Y[0].Count; k++)
                {
                    sum += -Y[i][k] * ((float)Math.Log(h[i][k])) - (1 - Y[i][k]) * ((float)Math.Log(1 - h[i][k]));
                }
            J *= sum + regularization;
            return J;
        } //Получение J для оценки правильности обучения
        private List<List<List<float>>> getDerivatives(List<List<float>> Y, List<List<float>> X, List<List<List<float>>> activations)
        {
            List<List<float>> delta = new List<List<float>>();
            List<List<List<float>>> D = new List<List<List<float>>>();
            for (int i = 0; i < _layers.Count; i++)
            {
                D.Add(new List<List<float>>());
                for (int j = 0; j < _layers[i].Weights.Count; j++)
                {
                    D[i].Add(new List<float>());
                    for (int k = 0; k < _layers[i].Weights[j].Count; k++)
                        D[i][j].Add(0);
                }
            }
            for (int k = 0; k < m; k++)
            {
                delta = new List<List<float>>();
                for (int i = 0; i < _layers.Count; i++)
                {
                    delta.Add(new List<float>());
                }
                for (int i = delta.Count - 1; i >= 0; i--)
                {
                    if (i == delta.Count - 1)
                        for (int j = 0; j < activations[k][i].Count; j++)
                            delta[delta.Count - 1].Add(activations[k][i][j] - Y[k][j]);
                    else
                    {
                        List<List<float>> traspWeight = transpose(_layers[i + 1].Weights);
                        delta[i] = matrixVectorMultiplay(delta[i + 1], traspWeight);
                        delta[i] = multiplayOnSigmoidGradient(delta[i], activations[k][i]);
                        D[i + 1] = sumOfMarixs(D[i + 1], vectorVectorMultiplay(delta[i + 1], activations[k][i]));
                    }
                    if (i == 0)
                    {
                        List<float> temp = new List<float>();
                        for (int t = 0; t < X[k].Count; t++)
                            temp.Add(X[k][t]);
                        D[i] = sumOfMarixs(D[i], vectorVectorMultiplay(delta[i], temp));
                    }
                }
            }
            List<List<List<float>>> derivatives = new List<List<List<float>>>();
            for (int i = 0; i < D.Count; i++)
            {
                derivatives.Add(multiplayMatrixNumber(D[i], (float)(1f / m)));
            }
            return derivatives;
        } //Получение частных производных
        private void gradientDescent(List<List<List<float>>> derivatives)
        {
            float regularizaton = 0;
            for (int l = 0; l < _layers.Count; l++)
            {
                for (int i = 0; i < _layers[l].Weights.Count; i++)
                {
                    for (int j = 0; j < _layers[l].Weights[i].Count; j++)
                    {
                        if (l > 0)
                        {
                            if (_layers[l - 1].Activations[i]==1)
                                regularizaton = 0;
                            else
                                regularizaton = lambda / m;
                        }
                        _layers[l].Weights[i][j] -= a * (derivatives[l][i][j] + _layers[l].Weights[i][j] * regularizaton);
                    }
                }
            }
        } //Градиентный спуск
        private List<float> multiplayOnSigmoidGradient(List<float> delta, List<float> activations)
        {
            List<float> tmp = new List<float>();
            for (int i = 1; i < delta.Count; i++)
                tmp.Add(delta[i] * activations[i] * (1 - activations[i]));
            return tmp;
        } //Умножение вектора на производную сигмоиды
        private void sigmoid(ref List<float> activation)
        {
            for (int i = 0; i < activation.Count; i++)
            {
                activation[i] = (float)(1 / (1 + Math.Exp(-activation[i])));
            }
        } //Получение сигмоды
        private List<List<float>> transpose(List<List<float>> A) //Транспонирование матрицы
        {
            List<List<float>> tmp = new List<List<float>>();
            for (int j = 0; j < A[0].Count; j++)
            {
                tmp.Add(new List<float>());
                for (int k = 0; k < A.Count; k++)
                {
                    tmp[j].Add(A[k][j]);
                }
            }
            return tmp;
        }
        private List<List<float>> matrixMatrixMultiplay(List<List<float>> A, List<List<float>> B)
        {
            List<List<float>> tmp = new List<List<float>>();
            for (int i = 0; i < A.Count; i++)
            {
                for (int j = 0; j < B[0].Count; j++)
                {
                    for (int k = 0; k < B.Count; k++)
                    {
                        tmp[i][j] += A[i][k] * B[k][j];
                    }
                }
            }
            return tmp;
        } //Умножение матрицы на матрицу
        private List<List<float>> multiplayMatrixNumber(List<List<float>> A, float numb) //Умножение матрицы на число
        {
            List<List<float>> tmp = new List<List<float>>();
            for (int i = 0; i < A.Count; i++)
            {
                tmp.Add(new List<float>());
                for (int j = 0; j < A[0].Count; j++)
                {
                    tmp[i].Add(A[i][j] * numb);
                }
            }
            return tmp;
        }
        private List<List<float>> sumOfMarixs(List<List<float>> A, List<List<float>> B) //Сложение двух матриц
        {
            List<List<float>> tmp = new List<List<float>>();
            for (int i = 0; i < A.Count; i++)
            {
                tmp.Add(new List<float>());
                for (int j = 0; j < A[0].Count; j++)
                {
                    tmp[i].Add(A[i][j] + B[i][j]);
                }
            }
            return tmp;
        }
        private List<List<float>> vectorVectorMultiplay(List<float> A, List<float> B)
        {
            List<List<float>> tmp = new List<List<float>>();
            for (int i = 0; i < A.Count; i++)
            {
                tmp.Add(new List<float>());
                for (int j = 0; j < B.Count; j++)
                {
                    tmp[i].Add(A[i] * B[j]);
                }
            }
            return tmp;
        } //Умножение вектора на вектор
        private List<float> matrixVectorMultiplay(List<float> inputs, List<List<float>> weights) //Умножение вектора на матрицу
        {
            List<float> tmp = new List<float>();
            for (int j = 0; j < weights.Count; j++)
            {
                tmp.Add(0);
                for (int k = 0; k < inputs.Count; k++)
                {
                    tmp[j] += inputs[k] * weights[j][k];
                }
            }
            return tmp;
        }
        public void StopLearning()
        {
            _stop = true;
        }
        public delegate void NewIteration(int iteration, float J);
        [field: NonSerialized]
        public event NewIteration OnNewIteration;
    }
}
